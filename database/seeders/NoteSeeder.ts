import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Note from 'App/Models/Note'

export default class extends BaseSeeder {
  public async run () {
    await Note.query().delete()

    await Note.create({
      title: "Kontrakan",
      content: "Ini catatan terkait kontrakan",
      user_id: 1,
    })

    for (let index = 1; index < 10; index++) {
      await Note.create({
        title: `Title ${index}`,
        content: `Content ${index}`,
        user_id: 1,
      })
    }
  }
}
