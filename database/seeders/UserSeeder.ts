import BaseSeeder from '@ioc:Adonis/Lucid/Seeder'
import Hash from '@ioc:Adonis/Core/Hash'
import User from 'App/Models/User'

export default class extends BaseSeeder {
  public async run () {
    for (let index = 1; index < 3; index++) {
      await User.create({
        username: `user${index}`,
        password: await Hash.make('qwerty'),
      })
    }
  }
}
