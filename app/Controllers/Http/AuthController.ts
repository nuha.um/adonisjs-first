import type { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Hash from '@ioc:Adonis/Core/Hash'
import User from 'App/Models/User'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {
    public async register({ request, response, auth }: HttpContextContract) {
        // get request
        const {username, password} = request.body()
        
        // check user
        const oldUser = await User.query().where({username:username}).first()
        if (oldUser) {
            return response.status(422).json({
                message: "Username sudah terdaftar"
            })
        }

        // make hashing password
        const hashedPassword = await Hash.make(password)

        // create user
        const user = await User.create({
            username:username,
            password: hashedPassword,
        })

        // generate token
        const token = await auth.use('api').generate(user)

        // return user
        return response.json({
            data: {
                user:user,
                token:token
            } 
        })

    }
    public async login({ request, response, auth }: HttpContextContract) {
        // get request
        const {username, password} = request.body()
        
        // check user
        const user = await User.query().where({username:username}).first()
        if (!user) {
            return response.status(422).json({
                message: "Username belum terdaftar"
            })
        }

        // verify password
        const verifyPassword = await Hash.verify(user.password,password)
        if (!verifyPassword) {
            return response.status(422).json({
                message: "Password tidak cocok"
            })
        }

        // generate token
        const token = await auth.use('api').generate(user,{
            expiresIn: '7 days'
        })

        // return user
        return response.status(200).json({
            data: {
                user:user,
                token:token
            } 
        })

    }

    public async tester({ response }: HttpContextContract) {
        const notes = await Database.rawQuery("SELECT data_id, milestone_id, data_master_id, data_bulan, data_tahun, 'not_change' as status, data_kolom_1, data_kolom_2, data_kolom_3, data_kolom_4, data_kolom_5, data_kolom_6, data_kolom_7 FROM data_master_data_utama WHERE milestone_id = 12 AND data_master_id = 444  AND unit_id = '1208'  AND data_bulan IN (1,2,3) AND data_tahun = 2023 AND data_is_deleted = 'no' ORDER BY data_id ASC LIMIT 10 OFFSET 0")
        return response.json({
            data: {
                notes: notes
            }
        })
    }
}
